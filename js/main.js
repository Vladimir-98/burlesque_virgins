
$(function () {
    $("#location").change(function () {
        var element = $(this).find('option:selected');
        var id = element.attr("id");
        if (id) {
            $("#online_phone").attr("placeholder", id);
        }
        console.log(id);
    });
});

$(document).ready(function () {
    loadUsers();
    $("#getAddUser").click(function () {
        var name = $("#name").val();
        var phone = $("#online_phone").val();
        var date = $("#date").val();
        $.ajax({
            method: "POST",
            url: "toadduser.php",
            data: {name: name, phone: phone, date: date},
            success: function (data) {
                var result = JSON.parse(data);
                if (result) {
                    $("." + result.alert).fadeIn().html(result.message);
                }
                setTimeout(function () {
                    $('.alert').fadeOut('fast');
                }, 2000);
                loadUsers();
            }
        });
    });
});




function loadUsers() {
    $.ajax({
        method: "POST",
        url: "gettousers.php",
        success: function (data) {
            var result = JSON.parse(data);
            var htmlUsers = "";
            var table = "";
            if (result == "") {
                $("#table").css('display', 'none');
            } else {
                for (i = 0; i < result.length; i++) {
                    htmlUsers +=
                        '<tbody>' +
                        '<tr>' +
                        '<th scope="row">' + result[i]['id'] + '</th>' +
                        '<td>' + result[i]['name'] + '</td>' +
                        '<td>' + result[i]['phone'] + '</td>' +
                        '<td>' + result[i]['date'] + '</td>' +
                        '<td>'+
                        '<button type="button" class="btn btn-danger deleteUser" id="' + result[i]['id'] + '">Удалить</button>' +
                        '</a>'+
                        '</td>' +
                        '</tr>' +
                        '</tbody>';
                }
                table =
                    '<table class="table table-bordered">' +
                    '<thead>' +
                    '<tr>' +
                    '<th scope="col">#</th>' +
                    '<th scope="col">имя</th>' +
                    '<th scope="col">телефон</th>' +
                    '<th scope="col">дата</th>' +
                    '</tr>' +
                    '</thead>' + htmlUsers + '' +
                    '</table>';
                $("#table").html(table);
                setTimeout(function () {
                    $('.table_data').fadeIn('fast');
                }, 2000);
            }
        }
    });
}
$(document).on("click", ".deleteUser", function() {
    var id = this.id;
    $.ajax({
        method: "POST",
        url: "delete.php",
        data: {id: id},
        success: function (data) {
            var result = JSON.parse(data);
            if (result) {
                $("." + result.alert).fadeIn().html(result.message);
            }
            setTimeout(function () {
                $('.alert').fadeOut('fast');
            }, 2000);
            loadUsers();
        }
    });
});
// $(document).ready(function () {
//     $('body').on('click', '#el', ...)
//     $('#deleteUser_5').on('click', function() {
//         console.log("YES");


//     });
// });

function setCursorPosition(pos, e) {
    e.focus();
    if (e.setSelectionRange) e.setSelectionRange(pos, pos);
    else if (e.createTextRange) {
        var range = e.createTextRange();
        range.collapse(true);
        range.moveEnd("character", pos);
        range.moveStart("character", pos);
        range.select()
    }
}

function mask(e) {
    var matrix = this.placeholder,
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");
    def.length >= val.length && (val = def);
    matrix = matrix.replace(/[_\d]/g, function (a) {
        return val.charAt(i++) || "_"
    });
    this.value = matrix;
    i = matrix.lastIndexOf(val.substr(-1));
    i < matrix.length && matrix != this.placeholder ? i++ : i = matrix.indexOf("_");
    setCursorPosition(i, this)
}

window.addEventListener("DOMContentLoaded", function () {
    var input = document.querySelector("#online_phone");
    input.addEventListener("input", mask, false);
    input.focus();
    setCursorPosition(3, input);
});
