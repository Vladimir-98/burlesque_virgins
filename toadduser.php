<?php

$result = "{\"error\":\"Что-то пошло не так!\"}";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['name']) && $_POST['name'] != "") {
        $name = htmlspecialchars($_POST['name']);
        if (isset($_POST['phone']) && $_POST['phone'] != "") {
            if (isset($_POST['date']) && $_POST['date'] != "") {
                require_once 'db.php';
                $user = getUser($_POST['phone']);
                if ($user == null) {
                    addUser($name, $_POST['date'], $_POST['phone']);
                    $result = "{\"message\":\"Ваши данные добавлены!\", \"alert\":\"alert-success\"}";
                } else {
                    $result = "{\"message\":\"Такой пользователь уже существует!\", \"alert\":\"alert-danger\"}";
                }
            } else {
                $result = "{\"message\":\"Вы не ввели дату рожденья!\", \"alert\":\"alert-danger\"}";
            }
        } else {
            $result = "{\"message\":\"Вы не ввели номер телефона!\", \"alert\":\"alert-danger\"}";
        }
    } else {
        $result = "{\"message\":\"Вы не ввели имя!\", \"alert\":\"alert-danger\"}";
    }
}

echo $result;

exit();

?>
