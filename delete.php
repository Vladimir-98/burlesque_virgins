<?php
$result = "{\"message\":\"Ошибка удаления!\", \"alert\":\"alert-danger\"}";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['id']) && is_numeric($_POST['id'])) {
        require_once 'User.php';
        $user = new User();
        $id = $_POST['id'];
        $userId = $user->getUserId($id);
        if ($user) {
            $user->delete($id);
            $result = "{\"message\":\"Данные удалены!\", \"alert\":\"alert-success\"}";
        }
    }
}
echo $result;
exit();

