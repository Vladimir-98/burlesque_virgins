<?php
try {
    $connection = new PDO("mysql:host=localhost;dbname=burlesque_virgins", "root", "");
} catch (PDOException $e) {
    echo $e->getMessage();
}

function addUser($name, $date, $phone)
{
    try {
        global $connection;
        $query = $connection->prepare(
            "
				INSERT INTO users (name, date, phone) 
				VALUES (:name, :date, :phone)
			"
        );
        $query->execute(array("name" => $name, "date" => $date, "phone" => $phone));

        return true;
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    return false;
}


function getUser($phone)
{
    global $connection;

    $result = null;

    try {
        $query = $connection->prepare(
            "
				SELECT * FROM users
				WHERE phone = :phone
				"
        );

        $query->execute(array("phone" => $phone));

        $result = $query->fetch();

        return $result;
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    return $result;
}

function getAllUsers()
{
    global $connection;

    $result = array();

    try {
        $query = $connection->prepare(
            "
				SELECT * FROM users ORDER BY id DESC
				"
        );

        $query->execute();

        $result = $query->fetchAll();

        return $result;
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    return $result;
}

?>
