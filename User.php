<?php

require_once 'DBManager.php';

class User
{
    private $dbManager;

    public function __construct(){
        $this->dbManager = new DBManager();
    }

    public function getUserId($id)
    {
        $result = null;

        try {
            $query = $this->dbManager->getConnection()->prepare(
                "
				SELECT * FROM users
				WHERE id = :id
				"
            );

            $query->execute(array("id" => $id));

            $result = $query->fetch();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $result;
    }


    public function delete($id)
    {
        try {
            $query = $this->dbManager->getConnection()->prepare(
                "
				DELETE FROM users WHERE id = :id
			"
            );

            $query->execute(
                array("id" => $id)
            );
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return false;
    }

}
