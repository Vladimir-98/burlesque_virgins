<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once 'db.php';
    $users = getAllUsers();
    echo json_encode($users);
    exit();
}
