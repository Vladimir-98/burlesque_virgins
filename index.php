<!DOCTYPE html>
<html>
<head>
    <?php
    require_once "head.php";
    ?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto mt-5">
            <div class="card">
                <form class="m-3" method="post" action="toadduser.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Имя:</label>
                        <input type="text" id="name" name="name" class="form-control" id="exampleInputName">
                    </div>
                    <div class="form-group">
                        <label for="phone">Номер телефона:</label>

                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="location">
                                    <option id="+7(___)___-__-__">Россия</option>
                                    <option id="+380(__)___-__-__">Украина</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" id="online_phone" name="phone" type="tel" minlength="16"
                                       maxlength="50" autofocus="autofocus" required="required"
                                       pattern="\+7\s?[\(]{0,1}9[0-9]{2}[\)]{0,1}\s?\d{3}[-]{0,1}\d{2}[-]{0,1}\d{2}"
                                       placeholder="+7(___)___-__-__">
                            </div>
                        </div>


                    </div>

                    <div class="form-group">
                        <label for="date">Дата рожденья:</label>
                        <div class="input_date" style="display: flex">
                            <input class="form-control" name="date" type="date" id="date" value="1990-01-04">
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary" id="getAddUser">Добавить</button>
                </form>
            </div>
            <div class="alert alert-success mt-3" role="alert" style="display: none"></div>
            <div class="alert alert-danger mt-3" role="alert" style="display: none"></div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 mx-auto table_data" id="table">
        </div>
    </div>
</div>
</body>
<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</html>

